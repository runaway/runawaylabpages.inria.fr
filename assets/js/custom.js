// This function queries the latest version from the repository and execute the
// `then` lambda on the version after that.
function get_last_version(then){
    const url = "https://gitlab.inria.fr/api/v4/projects/17733/repository/tags";
    $.getJSON(url, function(data){
        const version = data[0].name;
        then(version);
    });
}

// This function set the version number where needed in the page.
function set_version(v){
    $("span.version_number").text(v);
}

// Finally, when the document is ready. we execute the functions.
$(document).ready(function(){
    get_last_version(set_version);
})
