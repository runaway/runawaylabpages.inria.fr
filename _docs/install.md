---
layout: documentation
title: Installation
headline: Runaway ships as a compiled binary that you can execute directly. To have a working setup though, you will also have to install profiles to connect the remote resources you target.
index: 1
toc: true
---

# Installing the binary

Runaway is written in the [rust programming language](https://www.rust-lang.org/), which compile code into a binary form. This means that the run-time requirements are reduced compared to interpreted languages. This comes at the cost of supporting fewer platforms; currently we support the two main platforms used in academic research:

+ __x86_64 linux__
+ __x86_64 apple darwin__

Due to the wide use of proxy-commands to access remote clusters, `openssh` is a strong dependency of Runaway, and must be installed on your system, whatever the platform. We recomand that you use a recent version of `openssh>=8.0`:
```bash
# On debian/ubuntu 
$> sudo apt install openssh-client
# On fedora/rhel
$> sudo yum install openssh-clients
# On arch
$> sudo pacman -S openssh
# On mac
$> brew install openssh
```

### Linux

For the __linux__ architecture, we provide a convenient static binary that you can install without dependencies on your system. To install it, you just have to do the following:
```bash
$> curl -s {{site.linux_install}} \
   | bash /dev/stdin {{site.linux_link}}
```

You can now skip to the [installing profiles](#installing_profiles) section.

### Osx

For the __apple__ target, a few dependencies are needed. Make sure that you have [Homebrew](https://brew.sh) installed on your system and execute the following command:
```bash
$ curl -s {{site.osx_install}} \
  | bash /dev/stdin {{site.osx_link}}
```

### Other platforms

There is no official support for other platforms, but you probably can compile it on your own from the git repository:
```bash
# First install the rust compiler, using whatever is recommended on https://rustup.rs
# Then clone the repository
$> git clone https://gitlab.inria.fr/runaway/runaway.git
# Build the project
$> cd runaway && cargo build
```

# Installing profiles

To connect to remote hosts, Runaway uses profiles located in `~/.config/runaway`. Those profiles describe the location of the remote and the various procedures needed by Runaway to peform its job. You can write your own profile, but the vast majority of cases can be accomodated with the provided profile installers.

## Directly accessible resources

If the resource you target can be accessed directly, without any scheduler involved, then you can use the following script to install a profile that will be fitted to your needs. If you have no idea what a scheduler is, don't worry, it probably means that this profile is the right for you. Execute the following command in your terminal and fill the requirements as prompted:
```bash
$> python2 <(curl -s {{site.linux_direct_installer}})
```

__Note:__ To connect to remote hosts, Runaway stores the connection details in the same specification format used by _openssh_ in `~/.ssh/config` files. This script only generate simple configurations that need no more than one `Host` to connect. If you need more than one `Host` to access your resource, you can directly edit the `~/.config/runaway/config` file and append your configuration there.
{: .alert .alert-info}

## Slurm-managed resources

If the resources you target are managed by a slurm scheduler, you can use the following script to install a profile that will manage allocations on your behalf, and will allow you to use the cluster nodes as resources. To install it, you can use the following script:
```bash
$> python2 <(curl -s {{site.slurm_installer}})
```

__Note__: This installer provides a _parametric profiles_ which allows to tune the number of nodes you want at runtime. Check [execution model](/docs/execution) for more informations on that.
{: .alert .alert-info}
