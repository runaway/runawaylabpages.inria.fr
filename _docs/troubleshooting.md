---
layout: documentation
title: Troubleshooting
headline: To connect with remote resources, Runaway interfaces with a few system features. Things can go wrong when setup does not follow the expectation. Here are some details about common issues.
index: 999
toc: true
---

# Failure to connect to remote

To connect to remote resources, Runaway uses keys provided by an _ssh-agent_. An ssh-agent is a program responsible for storing authentication keys over multiple connections. For instance, if you have an encrypted key, it allows you to enter the key passphrase once for the whole session. In our case, we use it to be more flexible in authentication handling (you don't have to specify the right key for each resource).

The first consequence is that you need to be able to authenticate without password. If you were used to authenticating with a password, you can push one of your keys using the following command:
```bash
$> ssh-copy-id -i ~/.ssh/id_rsa_mykey some_remote
```

After that, you should be able to ssh into the remote without password prompt:
```bash
$> ssh some_remote
user@some_remote $> _
```

If it is not the case, it means that either your agent is inaccessible, or the agent does not have the right identity. To check if an ssh-agent is running try:
```bash
$> echo $SSH_AUTH_SOCK
  /run/user/1000/keyring/ssh
```
This should return the path to the ssh-agent socket. If nothing is returned, you have to start an ssh-agent. For linux users, you can find more informations on that at [this page](https://wiki.archlinux.org/index.php/SSH_keys#SSH_agents). 

If an agent is running, you can add your identity to the current ssh-agent by running:
```bash
$> ssh-add ~/.ssh/id_rsa_mykey
```

# Failure to start the script

If you installed your profile with one of the provided installers, chances are your remote execution environment is not set-up properly. In particular, Runaway uses _pseudo_-tty to communicate with hosts, which are usually detected in the `.bashrc` which returns prematurely, on purpose. 

To mitigate this issue, we provide another way to set-up your environment, which uses the `*.yaml` profile file. If you navigate to `~/.config/runaway` you should find one `.yaml` file for every profile you have installed. Every profile contains a `execution` section, which should start by a comment explaining how to add lines to setup your environment there. Basically, you just have to copy the relevant lines of your remote `.bashrc` in there, and you hsould be ready to go.
