```shell
$> runaway batch localhost train.py -A parameters.txt
  runaway: Loading host
  runaway: Reading arguments
  runaway: Reading ignore files
  runaway: Compress files
  runaway: Transferring data
  runaway: Starting execution with arguments "--lr=0.1"
  runaway: Starting execution with arguments "--lr=0.01"
  
  10eb724a-e9b4-441b-bbe2-e197a11d628d: Training model 
  da177f52-bb68-4799-8d82-6b511d557181: Training model
  
  ...
   
  runaway: Cleaning data on remote
$> 
```
