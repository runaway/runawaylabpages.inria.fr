```shell
$> runaway sched my-cluster train.py "./sched.py --reschedule"
  runaway: Loading host
  runaway: Reading arguments
  runaway: Reading ignore files
  runaway: Compress files
  runaway: Transferring data
  runaway: Querying the scheduler
  runaway: Starting execution with arguments "--lr=0.1 --no-augment "
 
  31908337-10bc-485a-806c-e3ef38cf6d1e: Training model
  ...
  
  runaway: Querying the scheduler
  runaway: Starting execution with arguments "--lr=0.01 "
  runaway: Starting execution with arguments "--lr=0.1 "
  runaway: Querying the scheduler
   
```
